#!/usr/bin/env python

import peewee
from peewee import *
from datetime import datetime
import hashlib
from passlib.hash import pbkdf2_sha256
from conf import *
"""
database.py

This is the database class that is used for all database operations within the application.

"""

# SELECT * FROM `user_order` JOIN `order` ON `user_order`.`order_id` = `order`.`id` where `user_order`.`user_id` = 5

# TODO: Subscription to newsletter for users(?)
# TODO: Bierabo(?)
# TODO: If user doesn't collect the order within 30 days we will refund 90% of the money he paid (Aufwandsentschädigung)

db = MySQLDatabase(host=getConfigValue("db_host"), user=getConfigValue("db_user"), passwd=getConfigValue("db_password"), database=getConfigValue("db_dbname"))

class BaseModel(Model):
    class Meta:
        database = db

class Beer(BaseModel):
    name = CharField(default="")
    description = CharField(default="")
    alc = DecimalField(default=0.0, decimal_places=2)
    img = CharField(default="")
    size = DecimalField(default=0.0, decimal_places=2)
    price = DecimalField(default=0.0, decimal_places=2)
    stock = SmallIntegerField(default=0)
    enabled = BooleanField(default=False)
    stripe_sku = CharField(default=None)
    stripe_prod = CharField(default=None)

class User(BaseModel):
    username = CharField(unique=True)
    password = CharField()
    first_name = CharField(default="")
    last_name = CharField(default="")
    email = CharField(default="")
    address = CharField(default="")
    address_add = CharField(default="")
    zipcode = SmallIntegerField(default=0000)
    state = CharField(default="")
    phone = CharField(default="")
    activated = BooleanField(default=False)
    has_newsletter = BooleanField(default=True)
    has_subscription = BooleanField(default=False)
    token = CharField()

class Order(BaseModel):
    order_date = DateField(default=datetime.now)
    order_id = CharField(unique=True)
    order_type = CharField()
    is_finished = BooleanField(default=False)
    order_note = CharField(default="")
    is_shipment = BooleanField(default=True)

class User_Order(BaseModel):
    user = ForeignKeyField(User)
    order = ForeignKeyField(Order, unique=True)

class Admin(BaseModel):
    username = CharField(unique=True)
    password = CharField()

# Create the required tables
def createTables():
    db.create_tables([Beer, User, Order, Admin, User_Order], safe=True)

# Get all items sorted ascending.
def getBeerList():
    return Beer.select().order_by(Beer.id.asc())

# Fetch beer by ID
def getBeerDetailsByID(id):
    return Beer.get(Beer.id == id)

# Get stock of beer by ID (Remove 6 as a buffer)
def getStockByID(id):
    return int(Beer.get(Beer.id == id).stock) - 6

# Validate User login
def validateUser(username, passwd):
    #pw = hashlib.sha224(password.encode("ASCII")).hexdigest()
    try:
        if not User.get(User.username == username) == None:
            hash = User.get(User.username == username).password
            return pbkdf2_sha256.verify(passwd, hash)
    except:
        return False
    return False


# Check if User is activated
def isUserActivated(username):
    try:
        if User.get(User.username == username).activated:
            return True
    except:
        return False
    return False

def activateUser(token):
    try:
        User.get(User.token == token)
        queryActivate = User.update(
            activated=True).where(User.token == token)
        queryActivate.execute()
        return True
    except:
        return False
    return False

# Get the user object from username
def getUserIDFromName(username):
    return User.get(User.username == username)

# Get orders for user
def getUserOrders(username, page=None):
    if not page == None:
        userID = getUserIDFromName(username).id
        # SELECT * FROM `user_order` JOIN `order` ON `user_order`.`order_id` = `order`.`id` where `user_order`.`user_id` = 5
        return (User_Order
        .select(Order.id, Order.order_id, Order.is_finished, Order.order_date, Order.order_type, Order.is_shipment)
        .join(Order)
        .switch(User_Order)
        .join(User)
        .where(User_Order.user_id == userID).order_by(Order.id.desc()).paginate(page, 10).dicts())
    else:
        userID = getUserIDFromName(username).id
        # SELECT * FROM `user_order` JOIN `order` ON `user_order`.`order_id` = `order`.`id` where `user_order`.`user_id` = 5
        return (User_Order
        .select(Order.id, Order.order_id, Order.is_finished, Order.order_date, Order.order_type, Order.is_shipment)
        .join(Order)
        .switch(User_Order)
        .join(User)
        .where(User_Order.user_id == userID).order_by(Order.id.desc()).dicts())

# Add order to user
def addUserOrder(paypal_id, username):
    userID = getUserIDFromName(username).id
    orderID = Order.get(Order.order_id == paypal_id).id
    try:
        User_Order.insert(user=userID, order=orderID).execute()
    except peewee.IntegrityError:
        pass

# Modify user account information
def modifyUserInformation(username, first_name, last_name, address, address_add, state, zipcode, has_newsletter):
    try:
        User.update(
                first_name=first_name,
                last_name=last_name,
                address=address,
                address_add=address_add,
                state=state,
                zipcode=zipcode,
                has_newsletter=has_newsletter
        ).where(User.username == username).execute()
        return True
    except:
        return False
    return False

# Modify user account password
def modifyUserPassword(username, old_pass, new_pass):
    try:
        if User.update(password=new_pass).where(User.username == username, User.password == old_pass).execute() != 1:
            return False
        return True
    except:
        return False
    return False

def removeFromStock(name, amount):
    stock = Beer.get(Beer.name == name).stock
    try:
        Beer.update(stock=stock-int(amount)).where(Beer.name == name).execute()
        if amount > stock:
            return False
        return True
    except:
        return False
    return False

def removeFromStockStripe(stripe_sku, amount):
    stock = Beer.get(Beer.stripe_sku == stripe_sku).stock
    try:
        Beer.update(stock=stock-int(amount)).where(Beer.stripe_sku == stripe_sku).execute()
        if amount > stock:
            return False
        return True
    except:
        return False
    return False



### ADMIN INTERFACE ###

# Add Order to DB for administration
def addOrder(o_id, o_type, shipment):
    try:
        Order.insert(order_id=o_id, order_type=o_type, is_shipment=shipment).execute()
    except peewee.IntegrityError:
        pass

# Get all open orders
def getOpenOrders():
    return Order.select().where(Order.is_finished == False).order_by(Order.id.desc())

# Paginated version
def getOpenOrdersPage(page):
    return Order.select().where(Order.is_finished == False).order_by(Order.id.desc()).paginate(page, 25)

# Get all finished orders
def getClosedOrders():
    return Order.select().where(Order.is_finished == True).order_by(Order.id.desc())

def getClosedOrdersPage(page):
    return Order.select().where(Order.is_finished == True).order_by(Order.id.desc()).paginate(page, 25)

# Get one order's details
def getOrderDetails(id):
    return Order.get(Order.id == id)

# Change status of order
def modifyOrder(id):
    status = Order.get(Order.id == id).is_finished
    if status:
        Order.update(is_finished = not status).where(Order.id == id).execute()
    else:
        Order.update(is_finished = not status).where(Order.id == id).execute()

# Modify Order Note
def modifyOrderNote(id, note):
    Order.update(order_note = note).where(Order.id == id).execute()

def validateLogin(username, password):
    pw = hashlib.sha224(password.encode("ASCII")).hexdigest()
    try:
        if not Admin.get(Admin.username == username, Admin.password == pw) == None:
            return True
    except:
        return False
    return False

def addToStockByID(id, amount):
    stock = Beer.get(Beer.id == id).stock
    Beer.update(stock=stock+int(amount)).where(Beer.id == id).execute()

def removeFromStockByID(id, amount):
    stock = Beer.get(Beer.id == id).stock
    Beer.update(stock=stock-int(amount)).where(Beer.id == id).execute()

def modifyBeer(id, name, description, alc, size, price, enabled):
    try:
        Beer.update(
            name = name,
            description = description,
            alc = alc,
            size = size,
            price = price,
            enabled = enabled
            ).where(Beer.id == id).execute()
        return True
    except:
        return False
    return False

def modifyBeerSKU(id, sku):
    try:
        Beer.update(
            stripe_sku=sku
            ).where(Beer.id == id).execute()
        return True
    except:
        return False
    return False

def modifyBeerImage(id, filepath):
    try:
        Beer.update(
            img=filepath
            ).where(Beer.id == id).execute()
        return True
    except:
        return False
    return False

def createBeer(name, descr, alc, size, price, stock, sku_id, prod_id):
    Beer.insert(name=name,
    description=descr,
    alc=alc,
    size=size,
    price=price,
    stock=stock,
    stripe_sku=sku_id,
    stripe_prod=prod_id
    ).execute()
### END ADMIN IFACE ###
