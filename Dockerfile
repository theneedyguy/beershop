FROM python:3.6
RUN mkdir -p /opt/shop

COPY ./app.py /opt/shop/
COPY ./database.py /opt/shop/
COPY ./utils.py /opt/shop/
COPY ./conf.py /opt/shop/
COPY ./config-sample.json /opt/shop/
COPY ./requirements.txt /opt/shop/
COPY ./templates /opt/shop/templates
COPY ./static /opt/shop/static
COPY ./fix-permissions /usr/bin/fix-permissions

RUN pip install -r /opt/shop/requirements.txt
RUN /usr/bin/fix-permissions /opt/shop/static && \
    /usr/bin/fix-permissions /opt/shop/static/images
WORKDIR /opt/shop

VOLUME ['/opt/shop/static/images']
USER nobody
EXPOSE 3054

ENTRYPOINT ["python3", "app.py"]
