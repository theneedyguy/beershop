#!/usr/bin/env python
from flask import Flask, session, url_for
import json
from validate_email import validate_email
import hashlib
from email.mime.text import MIMEText
import smtplib
import stripe
import uuid
from database import *
from conf import *


"""
util.py

This is the utility class which provides functions for various purposes.

"""

# Shipping costs
shipping_costs = 15
# Zipcodes that are free shipping (manual delivery)
free_shipping_zipcodes = ["3054", "3053"]

# Stripe API Config
stripe_keys = {
  'secret_key': getConfigValue("stripe_secret_key"),
  'publishable_key': getConfigValue("stripe_publishable_key")
}
stripe.api_key = stripe_keys['secret_key']

# Get the length of all items in basket (Used to show in indicator)
def getBasketSize():
    if "basket" in session:
        current_basket = session['basket']
        items = [x for x in current_basket["items"]]
    else:
        items = ""
    return len(items)

# Get Items in Basket. (Used for enum in Basket page)
def getBasketDetails():
    if "basket" in session:
        current_basket = json.loads(json.dumps(session["basket"]))
        basketDetails = [x for x in current_basket["items"]]
        return basketDetails
    return None

# Calculate the total costs of basket (Used in Basket page)
def getBasketTotal():
    if "basket" in session:
        current_basket = json.loads(json.dumps(session["basket"]))
        total = []
        for item in current_basket["items"]:
            total.append(item["qty"] * item["price"])
        return sum(total)
    return None

# Remove an item from basket (Used in Basket page)
def deleteBasketItem(id):
    if "basket" in session:
        current_basket = json.loads(json.dumps(session["basket"]))
        for item in current_basket["items"]:
            if item["id"] == int(id):
                current_basket["items"].remove(item)
        return current_basket
    return None

# Change amount of item in basket (Used in Basket page)
def modifyBasketItem(quantity, id):
    if quantity < 1:
        return deleteBasketItem(id)
    if getStockByID(id) < quantity:
        return False
    if "basket" in session:
        current_basket = json.loads(json.dumps(session["basket"]))
        for item in current_basket["items"]:
            if item["id"] == int(id):
                item["qty"] = int(quantity)
        return current_basket
    return False

# Get total amount of Bottles in basket (Used for validation on Basket page)
def getBasketAmount():
    if "basket" in session:
        current_basket = json.loads(json.dumps(session["basket"]))
        total = []
        for item in current_basket["items"]:
            total.append(item["qty"])
        return sum(total)
    return None

# Add to basket function. Adds items to basket if basket not in session
# and modifies existing items in session. (Used in Beer page)
def addToBasket(qty, id):
    beer = getBeerDetailsByID(id)
    if "basket" in session:
        current_basket = json.loads(json.dumps(session["basket"]))
        if int(id) not in [x["id"] for x in current_basket["items"]]:
            current_basket["items"].append({"id": beer.id, "name": beer.name,
                "price": float(beer.price) ,"qty": qty, "stripe_sku": beer.stripe_sku})
        else:
            for item in current_basket["items"]:
                if item["id"] == int(id):
                    if (item["qty"] + qty) > beer.stock - 6:
                        return False
                    else:
                        item["qty"] += qty
        return current_basket
    # Add new basket to session
    else:
        if not qty > beer.stock - 6:
            return {"items": [{"id": beer.id, "name": beer.name,
            "price": float(beer.price) ,"qty": qty, "stripe_sku": beer.stripe_sku}] }
        else:
            return False


def stripe_payment(name, surname, email, street, streetadd, city, postcode, token):
    if "basket" in session:
        for item in getBasketDetails():
            if getBeerDetailsByID(item["id"]).stock < int(item["qty"]):
                return None
        # Check if basket is still valid
        if getBasketAmount() % 6 != 0 or not getBasketAmount() > 0:
            return None
        current_basket = getBasketDetails()
        items = []
        for item in current_basket:
            items.append({"type": "sku" ,"parent": item["stripe_sku"], "quantity": int(item["qty"])})

        order = stripe.Order.create(
        currency='chf',
        items=items,
        shipping={
            "name": name + " " + surname,
            "address":{
              "line1": street,
              "line2": streetadd,
              "city": city,
              "country":'CH',
              "postal_code":postcode
            },
          },
        email=email
        )

        order.pay(source=token)
        return str(order)

def stripe_order(shipping_name, shipping_address_country, shipping_address_zip, shipping_address_state, shipping_address_line1, shipping_address_city, shipping_address_country_code, shipping_email,shipping_method_shop, token):
    if "basket" in session:
        for item in getBasketDetails():
            if getBeerDetailsByID(item["id"]).stock < int(item["qty"]):
                return None
        # Check if basket is still valid
        if getBasketAmount() % 6 != 0 or not getBasketAmount() > 0:
            return None
        if shipping_address_country_code != "CH":
            return None
        current_basket = getBasketDetails()
        items = []
        for item in current_basket:
            items.append({"type": "sku" ,"parent": item["stripe_sku"], "quantity": int(item["qty"])})
        order = stripe.Order.create(
            currency='chf',
            items=items,
            shipping={
                "name": shipping_name,
                "address":{
                  "line1": shipping_address_line1,
                  "city": shipping_address_city,
                  "country": shipping_address_country_code,
                  "postal_code":shipping_address_zip
                },
              },
            email=shipping_email
            )

        # This checks if the order is in range for free shipping and adjusts shipping accordingly
        # Also checks if user has selected to get the product personally
        if shipping_address_zip in free_shipping_zipcodes or shipping_method_shop == "Abholung":
            order = stripe.Order.retrieve(order["id"])
            for shipping_method in order["shipping_methods"]:
                if shipping_method["amount"] == 0:
                    order["selected_shipping_method"] = shipping_method["id"]
            order.save()
        # Else just leave order as is.


        order.pay(source=token)
        return str(order)

# Create Stripe Beer Product
def createBeerStripe(name, descr, price_sku):
    prod_uuid = str(uuid.uuid4())[:8]
    sku_uuid = str(uuid.uuid4())[:8]
    stripe_prod_uuid = "jj_prod_"+prod_uuid
    stripe_sku_uuid = "jj_sku_"+sku_uuid

    stripe.Product.create(
      id=stripe_prod_uuid,
      name=name,
      type="good"
    )

    stripe.SKU.create(
      id=stripe_sku_uuid,
      product=stripe_prod_uuid,
      price=int((price_sku*100)),
      currency="chf",
      inventory={
        "type": "infinite"
      }
    )
    return {"stripe_prod": stripe_prod_uuid, "stripe_sku": stripe_sku_uuid}

def modifyBeerStripe(sku, prod, name, price):
    product = stripe.Product.retrieve(prod)
    product.name = name
    product.save()

    sku = stripe.SKU.retrieve(sku)
    sku.price = int((price*100))
    sku.save()


# Validate E-Mail
def validateMail(email):
    return validate_email(email, verify=False)

def hashify(password):
    return hashlib.sha224(password.encode("ASCII")).hexdigest()

def hashpw(password):
    hashed = pbkdf2_sha256.hash(password)
    return hashed

def sendmail(receiver, token):
    server = smtplib.SMTP(getConfigValue("mail_host"), 587)
    server.ehlo()
    server.starttls()
    server.login(getConfigValue("mail_user"), getConfigValue("mail_password"))
    recipients = [receiver]
    sender = "no-reply@purpl3.net"
    subject = "JJ's Homebrew Aktivierungslink"
    body = """<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    </head>
	    <body>
	    <style>
	    </style>
	    <div>
		    <div class="container" style="padding:20px">
		    <h1>JJ's Homebrew Aktvierung</h1>
		    <p>Um dein Konto verwenden zu können, musst du es mit dem folgenden Link aktivieren.</p>
		    <p><a class="btn btn-primary btn-lg" href="http://shop.purpl3.net/user/activate?token=%s">Konto aktvieren&lt;</a></p>
		    </div>
	    </div>
	    </body>
    </html>""" % (token)
    msg = MIMEText(body.encode('utf-8'), 'html', 'UTF-8')
    msg['Subject'] = subject
    msg['From'] = sender
    msg['Content-Type'] = "text/html; charset=utf-8"
    msg['To'] = ", ".join(recipients)
    try:
        server.sendmail("no-reply@purpl3.net", receiver, str(msg))
    except:
        return
        server.quit()
