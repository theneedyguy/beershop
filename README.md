# Beershop

This app was written for JJ's Homebrew and is still under heavy development.
It's a custom web shop with Stripe integration. The aim is to be able to sell beer online via this custom web shop.

**You will most likely not have a use case for this shop since it is a custom application meant to be used in a certain environment.**
It was uploaded to GitHub for people to see and learn Python/Flask development and also for backup purposes.

# Requirements

- Linux host recommended
- MySQL Server installed and running
- Python3
- Docker Engine & Docker Compose (For running the Docker version)
- OpenShift (Optional)

First **rename** the **config-sample.json to config.json** and edit it to suit your needs.

---

## Standalone

The app can run standalone with python3 by installing the requirements via ```pip install -r requirements.txt``` and then running ```python3 app.py``` inside the project directory.

The app should then run on your machine on port **3054**. Connect to http://localhost:3054 to access the shop.

## Docker

There is already a working Docker Image on hub.docker.com/r/ckevi/jjbeershop containing a fully functioning version of the shop.

To run the application with Docker Compose you can use the existing docker-compose.yml file inside the project directory.
Just execute ```docker-compose up -d``` inside the project directory to run the shop inside a container.

_Make sure to mount the edited config.json file into your container to the path **/opt/shop/** or else the app will not work. (See the docker-compose.yml file for details). Alternatively you can set the environment variable SHOP_CONFIG with the path you want to mount the config.json to._

The app should then run on your machine on port **3054**. Connect to http://localhost:3054 to access the shop.

## OpenShift (Advanced)

_This is only for OpenShift users with advanced knowledge of the platform._

The Docker Image was crafted to run as a non-root user so it could work on an OpenShift cluster.

The following steps need to be taken to get the shop to work on OpenShift:

- Add a ConfigMap with the config.json to OpenShift and mount it to "/etc/shop" for example. **DO NOT MOUNT IT TO /opt/shop** (This will overwrite the /opt/shop folder and the application will not work.)
- Set the environment variable **SHOP_CONFIG** to the path of the mounted ConfigMap in the DeploymentConfig of the application.
- Either map an emptyDir to "/opt/shop/static/images" or create a PersistentVolumeClaim that points to this path.

## TODO

- Slack Integration for alerting on new order.
- Mail templates for order and verification.
- Logo and final design.
- Testing and bugfixing with testers.
- Ability to remove set images for beers.