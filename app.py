#!/usr/bin/env python
from flask import Flask, g, request, session, redirect, url_for , render_template, send_from_directory, escape, flash
# Protection
from flask_wtf.csrf import CSRFProtect
from flask_paranoid import Paranoid
# End Protection
from functools import wraps
from database import *
from utils import *
import json
from conf import *
import uuid
from werkzeug.utils import secure_filename
import os
app = Flask(__name__)
app.secret_key = getConfigValue("app_secret_key") #"$D5]xigkFtOb&H,FhiGOG<@DYeq{z6x+v-.=s=L8@+z5P-Wd>f+|]i]^-*TL>5U"
app.config["REMEMBER_COOKIE_SECURE"] = True
# SECURITY
# Protection for cross site request forgery
csrf = CSRFProtect(app)
csrf.init_app(app)
# Paranoid protection
paranoid = Paranoid(app)
paranoid.redirect_view = '/'
# END SECURITY
# IMG UPLOAD
app.config['ALLOWED_EXTENSIONS'] = set(['png', 'PNG', 'jpg', 'JPG', 'jpeg', 'JPEG'])
app.config['UPLOAD_FOLDER'] = './static/images/'
app.config['MAX_CONTENT_LENGTH'] = 16*1024*1024

def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']
# END IMG UPLOAD

def admin_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'admin' in session and session["admin"] == True:
            return f(*args, **kwargs)
        else:
            return redirect(url_for('admin_login'))
    return wrap

def user_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'username' in session:
            return f(*args, **kwargs)
        else:
            return redirect(url_for('user_login'))
    return wrap

@app.before_request
def startofrequest():
    g.db = db
    db.get_conn()

@app.teardown_request
def endofrequest(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()

@app.route("/beers")
def beers():
    error = request.args.get("error")
    success = request.args.get("success")
    beerList = getBeerList()
    basketSize = getBasketSize()
    return render_template("index.html",
        beers=beerList,
        title="Shop",
        basket_size=basketSize,
        error=error,
        success=success)

@app.route("/add_to_basket/<id>", methods=["POST"])
def add_to_basket(id):
    # Initalize error so no reference errors occur
    error = None
    error = request.args.get("error")
    # Get beer details by ID sent from request
    beer = getBeerDetailsByID(id)
    # Check stock of requested beer
    if beer.stock <= 6:
        error = "Dieses Bier ist nicht mehr an Lager."
        return redirect(url_for("beers", error=error))
    # Check if qty is more than one
    if int(request.form["quantity"]) >= 1:
        qty = int(request.form["quantity"])
    # Check if ordered quantity is more than stock
    if qty > beer.stock - 6:
        error = "Du bestellst mehr, als an Lager ist."
        return redirect(url_for("beers", error=error))
    # Call add to Basket function
    if addToBasket(qty, id):
        session["basket"] = addToBasket(qty, id)
    else:
        error = "Du bestellst mehr, als an Lager ist."
        return redirect(url_for("beers", error=error))
    return redirect(url_for("beers", error=error))


@app.route("/get_basket")
def get_basket():
    if "basket" in session:
        return str(session["basket"])
    return "Basket empty"


@app.route("/basket")
def basket():
    error = None
    error = request.args.get("error")
    if "basket" in session:
        basketSize = getBasketSize()
        basketDetails = getBasketDetails()
        basketTotal = getBasketTotal()
        basketAmount = getBasketAmount()
        return render_template("basket.html",
            title="Warenkorb",
            basket_size=basketSize,
            basketDetails=basketDetails,
            basketTotal=basketTotal,
            basketAmount=basketAmount,
            shipping=shipping_costs,
            error=error)
    return render_template("basket.html",
            title="Warenkorb",
            basket_size="0",
            basketDetails="",
            basketTotal="",
            basketAmount="",
            shipping="",
            error=error)

@app.route("/delete_basket/<id>")
def delete_basket(id):
    session["basket"] = deleteBasketItem(id)
    return redirect(url_for("basket"))

@app.route("/modify_basket/<id>", methods=["POST"])
def modify_basket(id):
    error = None
    error = request.args.get("error")
    if request.method == "POST":
        quantity = int(request.form["qty"])
        if modifyBasketItem(quantity, id):
            session["basket"] = modifyBasketItem(quantity, id)
            return redirect(url_for("basket"))
        error = "Du bestellst mehr, als an Lager ist."
        return redirect(url_for("basket", error=error))
    return "Bad Request",400

@app.route("/checkout")
def checkout():
    error = request.args.get("error")
    key = stripe_keys['publishable_key']
    if "basket" in session:
        if getBasketAmount() % 6 != 0 or not getBasketAmount() > 0:
            error = "Ungültiger Warenkorb."
            return redirect(url_for("beers", error=error))
        basketSize = getBasketSize()
        basketDetails = getBasketDetails()
        basketTotal = getBasketTotal()
        if "username" in session:
            return render_template("checkout.html",
            title="Checkout",
            basket_size=basketSize,
            basketDetails=basketDetails,
            user=getUserIDFromName(session["username"]),
            basketTotal=basketTotal,
            error=error,
            key=key)
        else:
            return render_template("checkout.html",
            title="Checkout",
            basket_size=basketSize,
            basketDetails=basketDetails,
            basketTotal=basketTotal,
            error=error,
            key=key)
    error = "Ungültige Anfrage."
    return redirect(url_for("beers", error=error))
    #return "Bad Request",400



@app.route("/stripe_pay", methods=["POST"])
def stripe_charge():
    error = request.args.get("error")

    token = request.form['stripeToken']
    shipping_name = request.form['stripeShippingName']
    shipping_address_country = request.form['stripeShippingAddressCountry']
    shipping_address_zip = request.form['stripeShippingAddressZip'].strip()
    shipping_address_state = request.form['stripeShippingAddressState']
    shipping_address_line1 = request.form['stripeShippingAddressLine1']
    shipping_address_city = request.form['stripeShippingAddressCity']
    shipping_address_country_code = request.form['stripeShippingAddressCountryCode']
    shipping_email = request.form['stripeEmail']
    shipping_method = request.form['delivery']

    if not validateMail(shipping_email):
        return redirect(url_for("checkout",
            error="Ungültige Mail"))

    payment = stripe_order(shipping_name,
    shipping_address_country,
    shipping_address_zip,
    shipping_address_state,
    shipping_address_line1,
    shipping_address_city,
    shipping_address_country_code,
    shipping_email,
    shipping_method,
    token)

    if payment == None:
        return redirect(url_for("checkout",
            error="Dein Warenkorb ist ungültig oder wir Liefern nicht in das gewählte Land."))

    payment_dump = json.loads(str(payment))
    if payment_dump["status"] == "paid":
        if shipping_method == "Abholung":
            addOrder(payment_dump["id"], "STRIPE", False)
        else:
            addOrder(payment_dump["id"], "STRIPE", True)
        if "username" in session:
            addUserOrder(payment_dump["id"], session["username"])
        items = payment_dump["items"]
        for item in items:
            if item["type"] == "sku":
                if not removeFromStockStripe(item["parent"], item["quantity"]):
                    session["basket"] = {"items": []}
                    return redirect(url_for("beers",
                        success="""Die Bestellung war erfolgreich,
                        jedoch ist direkt während deiner Bestellung der Lageranteil eines oder mehrerer Produkte ausgegangen.
                        Die Lieferung kann sich somit verzögern, da neues Bier gebraut werden muss."""))
    session["basket"] = {"items": []}
    return redirect(url_for("beers", success="Die Bestellung war erfolgreich!"))



@app.route("/")
def index():
    createTables()
    return redirect(url_for("beers"))

### User specific routes ###

@app.route("/login", methods=["GET", "POST"])
def user_login():
    error = None
    success = None
    error = request.args.get("error")
    success = request.args.get("success")
    if "user" in session:
        return redirect(url_for("user_profile"))
    if request.method == "POST":
        user = request.form["user"]
        pw = request.form["passwd"]
        if validateUser(user, pw):
            if isUserActivated(user):
                session["username"] = user
                return redirect(url_for("user_profile"))
            error = "Der Benutzer ist noch nicht aktiviert. Der Aktivierungslink wurde per Mail zugeschickt."
            return render_template("login.html", error=error, success=success)
        else:
            error = "Bentzername oder Passwort sind falsch."
            return render_template("login.html", error=error, success=success)
    return render_template("login.html", error=error, success=success)

@app.route("/user")
@user_required
def user_profile():
    basketSize = getBasketSize()
    username = session["username"]
    return render_template("user.html", basket_size=basketSize)
    #return "User Profile %s" % (getUserOrders(username))

@app.route("/user/config")
@user_required
def user_config():
    error = request.args.get("error")
    success = request.args.get("success")
    basketSize = getBasketSize()
    username = session["username"]
    return render_template("user_config.html", basket_size=basketSize,
        user=getUserIDFromName(username), error=error, success=success)
    #return "User Profile %s" % (getUserOrders(username))

@app.route("/user/config/save", methods=["POST"])
@user_required
def user_config_save():
    basketSize = getBasketSize()
    username = session["username"]
    name = request.form["name"].strip()
    surname = request.form["surname"].strip()
    street = request.form["street"].strip()
    streetadd = request.form["streetadd"].strip()
    city = request.form["city"].strip()
    postcode = request.form["postcode"].strip() if request.form["postcode"].strip() != "" else 0
    newsletter = "newsletter" in request.form
    if modifyUserInformation(username, name, surname, street, streetadd, city, postcode, newsletter):
        success = "Die Änderungen wurden erfolgreich übernommen"
        return redirect(url_for("user_config", success=success))
    else:
        error = "Die Änderungen konnten nicht gespeichert werden."
        return redirect(url_for("user_config", error=error))
    return redirect(url_for("user_config",error=error, success=success))

@app.route("/user/config/changepass", methods=["POST"])
@user_required
def user_config_change_pw():
    error = None
    success = None
    basketSize = getBasketSize()
    username = session["username"]
    old_pw = hashify(request.form["old"])
    new_pw = hashify(request.form["new1"])
    new_pw2 = hashify(request.form["new2"])
    if not len(request.form["new1"]) >= 8 or not len(request.form["new2"]) >=8:
        error = "Passwort ist nicht lang genug. (Mindestens 8 Zeichen)"
        return redirect(url_for("user_config", error=error))
    if not new_pw2 == new_pw:
        error = "Die neuen Passwörter stimmen nicht überein."
        return redirect(url_for("user_config", error=error))
    if not modifyUserPassword(username, old_pw, new_pw):
        error = "Das alte Passwort ist falsch."
        return redirect(url_for("user_config", error=error))
    else:
        success = "Das Passwort wurde erfolgreich geändert."
    return redirect(url_for("user_config", error=error, success=success))

@app.route('/user/orders', defaults={'page':1})
@app.route("/user/orders/<int:page>")
@user_required
def user_orders(page):
    username = session["username"]
    return render_template("orders.html", basket_size=getBasketSize(),
        userOrders=getUserOrders(username, page), page=page)

@app.route("/user/orders/details/<id>")
@user_required
def user_order_details(id):
    # IF order.id in array then allow user to see it (security) --> Details
    username = session["username"]
    if int(id) not in [int(x["id"]) for x in getUserOrders(username)]:
        return "Not allowed to see other users orders. %s" % (str([x["id"] for x in getUserOrders(username)]))

    orderDetails = getOrderDetails(id)
    # STRIPE VIEW
    order = json.loads(str(stripe.Order.retrieve(orderDetails.order_id)))
    total_cost = order["amount"]
    payer_email = order["email"]
    payer_name = order["shipping"]["name"]
    payer_postcode = order["shipping"]["address"]["postal_code"]
    payer_city = order["shipping"]["address"]["city"]
    payer_line1 = order["shipping"]["address"]["line1"]
    items = []
    for item in order["items"]:
        if item["type"] == "sku" or item["type"] == "shipping":
            items.append(item)

    paymentObject = {"total_cost": total_cost, "items": items,
         "payer_email": payer_email, "payer_name": payer_name, "payer_postcode": payer_postcode,
         "payer_city": payer_city, "payer_line1": payer_line1}

    return render_template("order_details.html", basket_size=getBasketSize(),
            orderDetails=orderDetails, paymentObject=paymentObject)


@app.route("/logout")
@user_required
def user_logout():
    session.pop("username")
    return redirect(url_for("user_login"))

@app.route("/user/activate")
def activate():
    error = None
    if not request.args == None:
        token = request.args.get('token')
        if activateUser(token):
            return redirect(url_for("user_login", success="Der Benutzer wurde erfolgreich aktiviert. Du kannst duch nun einloggen."))
        else:
            return redirect(url_for("beers", error="Ungültiges Aktivierunstoken"))
    return redirect(url_for("beers", error="Ungültiges Aktivierunstoken"))

@app.route("/signup", methods=["GET", "POST"])
def user_signup():
    error = None
    success = None
    if request.method == "POST":
        user = request.form["username"]
        pw = request.form["pw"]
        pw2 = request.form["pw2"]
        email = request.form["email"]
        token = str(uuid.uuid4())
        if not len(user) >= 3:
            error = "Benutzername ist zu kurz."
            return render_template("signup.html", error=error, success=success)
        if not validateMail(email):
            error = "Ungültige Email."
            return render_template("signup.html", error=error, success=success)
        if not len(request.form["pw"]) >= 8 or not len(request.form["pw2"]) >= 8:
            error = "Passwort ist zu kurz. Das Passwort muss mindestens 8 Zeichen lang sein."
            return render_template("signup.html", error=error, success=success)
        if not pw2 == pw:
            error = "Die Passwörter stimmen nicht überein."
            return render_template("signup.html", error=error, success=success)
        try:
            newUser = User(
                username=user, password=hashpw(pw), email=email, token=token)
            newUser.save()
        except:
            error = "Dieser Benutzer konnte nicht angelegt werden, da bereits ein Benutzer mit diesem Namen existiert."
            return render_template("signup.html", error=error, success=success)
        success = "Benutzer wurde erstellt. Bitte aktiviere den Benutzer mit dem Link, der an deine E-Mail Adresse gesendet wurde."
        sendmail(email, token)
        return render_template("signup.html", error=error, success=success)
    return render_template("signup.html", error=error, success=success)

### END User specific routes ###

### ADMIN INTERFACE ###

@app.route("/admin", methods=["GET", "POST"])
def admin_login():
    if "admin" in session:
        return redirect(url_for("admin_open_orders"))
    if request.method == "POST":
        user = request.form["user"]
        pw = request.form["passwd"]
        if validateLogin(user, pw):
            session["admin"] = True
            return redirect(url_for("admin_open_orders"))
        error = "Benutzername oder Passwort sind falsch."
        return render_template("admin/login.html", error=error)
    return render_template("admin/login.html")

@app.route("/admin/logout")
@admin_required
def admin_logout():
    session.pop("admin")
    return redirect(url_for("admin_login"))

@app.route('/admin/orders/open', defaults={'page':1})
@app.route("/admin/orders/open/<int:page>")
@admin_required
def admin_open_orders(page):
    return render_template("admin/open.html", openOrders=getOpenOrdersPage(page), page=page)

@app.route('/admin/orders/closed', defaults={'page':1})
@app.route("/admin/orders/closed/<int:page>")
@admin_required
def admin_closed_orders(page):
    return render_template("admin/closed.html", closedOrders=getClosedOrdersPage(page), page=page)

@app.route("/admin/orders/details/<id>")
@admin_required
def admin_order_details(id):
    orderDetails = getOrderDetails(id)

    # STRIPE VIEW
    order = json.loads(str(stripe.Order.retrieve(orderDetails.order_id)))
    total_cost = order["amount"]
    payer_email = order["email"]
    payer_name = order["shipping"]["name"]
    payer_postcode = order["shipping"]["address"]["postal_code"]
    payer_city = order["shipping"]["address"]["city"]
    payer_line1 = order["shipping"]["address"]["line1"]
    items = []
    for item in order["items"]:
        if item["type"] == "sku" or item["type"] == "shipping":
            items.append(item)

    paymentObject = {"total_cost": total_cost, "items": items,
         "payer_email": payer_email, "payer_name": payer_name, "payer_postcode": payer_postcode,
         "payer_city": payer_city, "payer_line1": payer_line1}

    return render_template("admin/details.html", orderDetails=orderDetails,
        paymentObject=paymentObject)

@app.route("/admin/beers")
@admin_required
def admin_beers():
    return render_template("admin/beers.html", beerList=getBeerList())

@app.route("/admin/beers/details/<id>")
@admin_required
def admin_beer_details(id):
    error = request.args.get("error")
    success = request.args.get("success")
    return render_template("admin/beer_details.html",
        beer=getBeerDetailsByID(id), error=error, success=success)

@app.route("/admin/beer/stock_add/<id>", methods=["POST"])
@admin_required
def admin_beer_add_stock(id):
    amount = int(request.form["add"])
    addToStockByID(id, amount)
    return redirect(url_for("admin_beer_details", id=id))

@app.route("/admin/beer/stock_remove/<id>", methods=["POST"])
@admin_required
def admin_beer_remove_stock(id):
    amount = int(request.form["remove"])
    if amount > int(getBeerDetailsByID(id).stock):
        return redirect(url_for("admin_beer_details", id=id,
            error="Du versuchst mehr Stock zu entfernen, als vorhanden ist."))
    removeFromStockByID(id, amount)
    return redirect(url_for("admin_beer_details", id=id))

@app.route("/admin/beer/modify/<id>", methods=["POST"])
@admin_required
def admin_beer_modify(id):
    beer = getBeerDetailsByID(id)
    name = request.form["name"].strip()
    price = request.form["price"].strip()
    alc = request.form["alc"].strip()
    size = request.form["size"].strip()
    active = "active" in request.form
    description = request.form["description"].strip()
    if modifyBeer(id, name, description, alc, size, price, active):
        modifyBeerStripe(beer.stripe_sku, beer.stripe_prod, name, float(price))
        return redirect(url_for("admin_beer_details", id=id,
            success="Details wurden geändert"))
    return redirect(url_for("admin_beer_details", id=id,
        error="Es ist ein Fehler aufgetreten."))

@app.route("/admin/beer/sku/modify/<id>", methods=["POST"])
@admin_required
def admin_beer_sku(id):
    sku = request.form["sku"].strip()
    if modifyBeerSKU(id, sku):
        return redirect(url_for("admin_beer_details",
            id=id,
            success="Details wurden geändert"))
    return redirect(url_for("admin_beer_details",
        id=id,
        error="Es ist ein Fehler aufgetreten."))

@app.route("/admin/beer/img/<id>", methods=["POST"])
@admin_required
def admin_beer_image(id):
    file = request.files["file"]
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        if modifyBeerImage(id, str(os.path.join(app.config['UPLOAD_FOLDER'], filename))):
            return redirect(url_for("admin_beer_details",
                id=id,
                success="Bild wurde geändert"))
    return redirect(url_for("admin_beer_details",
        id=id,
        error="Es ist ein Fehler aufgetreten."))

@app.route("/admin/orders/get_payment/<id>")
@admin_required
def admin_get_payment(id):
    return str(paypalrestsdk.Payment.find(id))

@app.route("/admin/orders/modify/<id>", methods=["POST"])
@admin_required
def admin_order_modify(id):
    modifyOrder(id)
    return redirect(url_for("admin_order_details",id=id))

@app.route("/admin/orders/modify_note/<id>", methods=["POST"])
@admin_required
def admin_order_note_modify(id):
   notes = request.form["notes"]
   modifyOrderNote(id, notes)
   return redirect(url_for("admin_order_details",id=id))

@app.route("/admin/orders/find", methods=["POST"])
@admin_required
def admin_order_find():
    order_id = int(request.form["order"])
    try:
        result = getOrderDetails(order_id)
    except:
        result = False
    return render_template("admin/find.html", result=result)

@app.route("/admin/beer/add")
@admin_required
def admin_beer_add():
    return render_template("/admin/beer_add.html")

@app.route("/admin/beer/create", methods=["POST"])
@admin_required
def admin_beer_create():
    name = request.form["name"].strip()
    price = request.form["price"].strip()
    alc = request.form["alc"].strip()
    size = request.form["size"].strip()
    stock = request.form["stock"].strip()
    description = request.form["description"]
    ids = createBeerStripe(name, description, float(price))
    createBeer(name, description, alc, size, price, stock, ids["stripe_sku"], ids["stripe_prod"])
    return redirect(url_for("admin_beers", success="Bier wurde erstellt."))

### END ADMIN IFACE ###

if __name__ == "__main__":
    app.run(host='0.0.0.0', port='3054')
